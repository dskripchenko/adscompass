<?php


namespace App\Api\Components;


use App\Api\Versions\v1\Api as ApiV1;
use Dskripchenko\LaravelApi\Components\BaseModule;

class ApiModule extends BaseModule
{
    /**
     * @return string[]
     */
    public function getApiVersionList()
    {
        return [
            'v1' => ApiV1::class
        ];
    }

    /**
     * @return string
     */
    public function getApiPrefix()
    {
        return '41d5dba9711674ee9b5b68a2eba28ef7';
    }
}
