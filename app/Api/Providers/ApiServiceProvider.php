<?php


namespace App\Api\Providers;

use App\Api\Components\ApiModule;
use \Dskripchenko\LaravelApi\ApiServiceProvider as ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * @return ApiModule
     */
    protected function getApiModule()
    {
        return new ApiModule();
    }

}
