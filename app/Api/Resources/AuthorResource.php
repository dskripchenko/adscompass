<?php


namespace App\Api\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class AuthorResource extends JsonResource
{
    /**
     * @var bool
     */
    protected $withBooks = false;

    /**
     * @return $this
     */
    public function withBooks(): AuthorResource
    {
        $this->withBooks = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function withoutBooks(): AuthorResource
    {
        $this->withBooks = false;
        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $result = [
            'id' => $this->id,
            'name' => $this->name
        ];

        if($this->withBooks) {
            $result['books'] = BookResource::collection($this->books);
        }

        return $result;
    }

}
