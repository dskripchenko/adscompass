<?php


namespace App\Api\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * @var bool
     */
    protected $withAuthors = false;

    protected $withComments = false;

    /**
     * @return $this
     */
    public function withAuthors(): BookResource
    {
        $this->withAuthors = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function withoutAuthors(): BookResource
    {
        $this->withAuthors = false;
        return $this;
    }

    /**
     * @return $this
     */
    public function withComments(): BookResource
    {
        $this->withComments = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function withoutComments(): BookResource
    {
        $this->withComments = false;
        return $this;
    }

    public function toArray($request)
    {
        $result = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
        ];

        if($this->withAuthors) {
            $result['authors'] = AuthorResource::collection($this->authors);
        }

        if($this->withComments) {
            $result['comments'] = CommentResource::collection($this->comments()->limit(10)->get());
        }

        return $result;
    }

}
