<?php


namespace App\Api\Versions\v1;


use App\Api\Versions\v1\Controllers\AuthorController;
use App\Api\Versions\v1\Controllers\BookController;
use App\Api\Versions\v1\Controllers\CommentController;
use Dskripchenko\LaravelApi\Components\BaseApi;

class Api extends BaseApi
{
    protected static function getMethods()
    {
        return [
            'controllers' => [
                'author' => [
                    'controller' => AuthorController::class,
                    'actions' => [
                        'create',
                        'info',
                        'delete'
                    ]
                ],
                'book' => [
                    'controller' => BookController::class,
                    'actions' => [
                        'create',
                        'info',
                        'search',
                        'delete',
                        'add-author' => 'addAuthor'
                    ]
                ],
                'comment' => [
                    'controller' => CommentController::class,
                    'actions' => [
                        'create',
                        'list' => 'getList'
                    ]
                ]
            ],
            'middleware' => []
        ];
    }
}
