<?php


namespace App\Api\Versions\v1\Controllers;


use App\Api\Resources\AuthorResource;
use App\Models\Author;
use Dskripchenko\LaravelApi\Components\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthorController extends ApiController
{
    /**
     * Добавить автора
     *
     * @input string $name Имя автора
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string|min:3|max:128'
        ]);

        return $this->success(new AuthorResource(Author::create($request->name)));
    }

    /**
     * Получить информацию об авторе
     *
     * @input integer $id Идентификатор автора
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function info(Request $request): JsonResponse
    {
        $request->validate([
            'id' => 'required|integer'
        ]);

        $author = Author::query()->with('books')->findOrFail($request->id);

        return $this->success((new AuthorResource($author))->withBooks());
    }

    /**
     * Удалить автора
     *
     * @input integer $id Идентификатор автора
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $request->validate([
            'id' => 'required|integer'
        ]);

        $author = Author::query()->findOrFail($request->id);

        $author->delete();

        return $this->success();
    }
}
