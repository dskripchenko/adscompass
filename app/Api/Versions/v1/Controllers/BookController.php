<?php


namespace App\Api\Versions\v1\Controllers;


use App\Api\Resources\BookResource;
use App\Models\Author;
use App\Models\Book;
use Dskripchenko\LaravelApi\Components\ApiController;
use Dskripchenko\LaravelApi\Components\ApiException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BookController extends ApiController
{
    /**
     * Добавить книгу
     *
     * @input integer $authorId Идентификатор автора
     * @input string $name Название книги
     * @input string $description описание книги
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $request->validate([
            'authorId' => 'required|integer',
            'name' => 'required|string|min:3|max:128',
            'description' => 'required|string|min:3|max:1024',
        ]);

        $author = Author::query()->findOrFail($request->authorId);

        return $this->success(new BookResource(Book::create($author, $request->name, $request->description)));
    }

    /**
     * Получить информацию о книге
     *
     * @input integer $id Идентификатор книги
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function info(Request $request): JsonResponse
    {
        $request->validate([
            'id' => 'required|integer'
        ]);

        $book = Book::query()->with('authors', 'comments')->findOrFail($request->id);

        return $this->success((new BookResource($book))->withAuthors()->withComments());
    }

    /**
     * Удалить книгу
     *
     * @input integer $id Идентификатор книги
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $request->validate([
            'id' => 'required|integer'
        ]);

        $book = Book::query()->findOrFail($request->id);

        $book->delete();

        return $this->success();
    }

    /**
     * Получить список книг по автору/авторам
     *
     * @input string $ids Идентификаторы авторов разделенные запятой (1,2,3,4,77)
     * @input integer $page Номер страницы
     * @input integer ?$perPage Количество записей на страницу (минимум 1, максимум 100, по умолчанию 10)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $request->validate([
            'ids' => 'required|string|regex:/^[\d,]+$/',
            'page' => 'required|integer|min:1',
            'perPage' => 'integer|min:1|max:100',
        ]);

        $ids = explode(',', $request->ids);
        $ids = array_unique($ids);
        $ids = array_filter($ids, function ($id) {
            return is_numeric($id) && $id > 0;
        });

        if(empty($ids)) {
            throw new ApiException('invalid_ids', 'Некорректный список идентификаторов авторов');
        }

        $query = Book::query()
            ->join('book2authors', 'book2authors.book_id', '=', 'books.id')
            ->whereIn('book2authors.author_id', $ids);

        $paginator = $query->paginate($request->get('perPage', 10));

        return $this->success([
            'items' => BookResource::collection($paginator->items()),
            'meta' => [
                'page' => $paginator->currentPage(),
                'perPage' => $paginator->perPage(),
                'lastPage' => $paginator->lastPage(),
                'total' => $paginator->total()
            ]
        ]);
    }

    /**
     * Добавить соавтора
     *
     * @input integer $id Идентификатор книги
     * @input integer $authorId Идентификатор автора
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addAuthor(Request $request): JsonResponse
    {
        $request->validate([
            'id' => 'required|integer',
            'authorId' => 'required|integer'
        ]);

        $book = Book::query()->with('authors')->findOrFail($request->id);
        $author = Author::query()->findOrFail($request->authorId);
        if(!$book->authors()->where('authors.id', $author->id)->exists()) {
            $book->authors()->save($author);
            $book->refresh();
        }

        return $this->success((new BookResource($book))->withAuthors());
    }
}
