<?php


namespace App\Api\Versions\v1\Controllers;


use App\Api\Resources\CommentResource;
use App\Models\Book;
use App\Models\Comment;
use Dskripchenko\LaravelApi\Components\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends ApiController
{
    /**
     * Добавить комментарий
     *
     * @input integer $bookId Идентификатор книги
     * @input string $message Содержание комментария
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $request->validate([
            'bookId' => 'required|integer',
            'message' => 'required|string|min:1|max:1024',
        ]);

        $book = Book::query()->findOrFail($request->bookId);

        return $this->success(new CommentResource(Comment::create($book, $request->message)));
    }

    /**
     * Получить список комментариев
     *
     * @input string $bookId Идентификатор книги
     * @input integer $page Номер страницы
     * @input integer ?$perPage Количество записей на страницу (минимум 1, максимум 100, по умолчанию 10)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        $request->validate([
            'bookId' => 'required|integer',
            'page' => 'required|integer|min:1',
            'perPage' => 'integer|min:1|max:100',
        ]);

        $book = Book::query()->findOrFail($request->bookId);

        $query = Comment::query()->where('book_id', $book->id);

        $paginator = $query->paginate($request->get('perPage', 10));

        return $this->success([
            'items' => CommentResource::collection($paginator->items()),
            'meta' => [
                'page' => $paginator->currentPage(),
                'perPage' => $paginator->perPage(),
                'lastPage' => $paginator->lastPage(),
                'total' => $paginator->total()
            ]
        ]);
    }
}
