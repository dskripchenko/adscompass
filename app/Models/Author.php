<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;

    protected $visible = ['id'];

    /**
     * @return HasMany
     */
    public function book2authors(): HasMany
    {
        return $this->hasMany(Book2author::class);
    }

    /**
     * @return BelongsToMany
     */
    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class, 'book2authors');
    }

    /**
     * @param string $name
     * @return static
     */
    public static function create(string $name): Author
    {
        $instance = new static();
        $instance->name = $name;
        $instance->save();
        return $instance;
    }
}
