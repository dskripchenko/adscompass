<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $visible = ['id'];

    /**
     * @return HasMany
     */
    public function book2authors(): HasMany
    {
        return $this->hasMany(Book2author::class);
    }

    /**
     * @return BelongsToMany
     */
    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(Author::class, 'book2authors');
    }

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @param Author $author
     * @param string $name
     * @param string $description
     * @return Book
     */
    public static function create(Author $author, string $name, string $description): Book
    {
        $instance = new static();
        $instance->name = $name;
        $instance->description = $description;
        $instance->save();

        $instance->authors()->save($author);

        return $instance;
    }
}
