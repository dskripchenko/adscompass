<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $visible = ['id'];

    /**
     * @return HasOne
     */
    public function book(): HasOne
    {
        return $this->hasOne(Book::class);
    }

    /**
     * @param Book $book
     * @param string $message
     * @return static
     */
    public static function create(Book $book, string $message): Comment
    {
        $instance = new static();
        $instance->message = $message;
        $instance->book_id = $book->id;
        $instance->save();
        return $instance;
    }
}
